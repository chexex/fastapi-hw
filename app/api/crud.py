from typing import List, Optional

from app.core.db import comments, posts, database


async def get_post_by_id(id_: int):
    query = posts.select().where(posts.c.id == id_)
    return await database.fetch_one(query=query)


async def get_comments_by_post_id(post_id: int):
    query = comments.select().where(comments.c.post_id == post_id)
    return await database.fetch_all(query=query)


async def get_posts(offset: int = 0, limit: int = 10):
    return await database.fetch_all(query=posts.select().offset(offset).limit(limit))


async def get_comments(posts_ids: Optional[List[int]] = None):
    q = comments.select()
    if posts_ids:
        q = q.where(comments.c.post_id.in_(posts_ids))
    return await database.fetch_all(query=q)
