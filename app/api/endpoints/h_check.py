from fastapi import APIRouter

router = APIRouter()


@router.get('/h-check')
async def h_check():
    return {'status': 'OK'}
