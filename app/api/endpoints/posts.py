from collections import defaultdict
from typing import Dict, List

from fastapi import APIRouter, HTTPException, Path
from fastapi_cache.decorator import cache

from app.api import crud
from app.api.models import CommentDB, PostDB, PostWithCommentsDB

router = APIRouter()


def get_post_to_comments(comments: List[CommentDB]) -> Dict[int, List[CommentDB]]:
    """
    Normalize comments array to `post.id: comments` object
    """
    post_to_comments = defaultdict(list)
    for comment in comments:
        post_to_comments[comment.post_id].append(comment)
    return post_to_comments


def get_post_with_comments(post: PostDB, comments: List[CommentDB]) -> PostWithCommentsDB:
    """
    Union post and its comments
    """
    return PostWithCommentsDB(**post.dict(), comments=comments)


@router.get("/", response_model=List[PostWithCommentsDB])
@cache(expire=60)
async def get_posts(offset: int = 0, limit: int = 10):
    """
    Get all available posts with appropriate comments
    """
    out = []
    posts_db = await crud.get_posts(offset, limit)
    posts = [PostDB(**p) for p in posts_db]

    comments_db = await crud.get_comments([p.id for p in posts])
    comments = [CommentDB(**c) for c in comments_db]

    post_to_comments = get_post_to_comments(comments)

    for post in posts:
        out.append(get_post_with_comments(post, post_to_comments[post.id]).dict())

    return out


@router.get("/{id}/", response_model=PostWithCommentsDB)
@cache(expire=60)
async def get_post_by_id(id: int = Path(..., gt=0)):
    """
    Get post by ID with appropriate comments
    """
    post_db = await crud.get_post_by_id(id)

    if not post_db:
        raise HTTPException(status_code=404, detail="Post not found")
    post = PostDB(**post_db)
    comments = await crud.get_comments_by_post_id(post.id)
    return get_post_with_comments(post, comments).dict()
