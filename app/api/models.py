from typing import List

from pydantic import BaseModel, Field


class PostSchema(BaseModel):
    user_id: int
    title: str = Field(..., max_length=255)
    body: str


class CommentSchema(BaseModel):
    post_id: int
    name: str = Field(..., max_length=255)
    email: str = Field(..., max_length=255)
    body: str


class PostWithCommentsSchema(PostSchema):
    comments: List[CommentSchema]


class PostDB(PostSchema):
    id: int


class CommentDB(CommentSchema):
    id: int


class PostWithCommentsDB(PostDB):
    comments: List[CommentDB]
