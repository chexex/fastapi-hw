from pydantic import BaseSettings


class Config(BaseSettings):
    service_name: str = "posts_test"
    secret_key: str = "not-so-secret"
    pg_dsn: str = "postgres://user:pass@db:5432/posts"
    redis_dsn: str = "redis://redis"


config = Config()
