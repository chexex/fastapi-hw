import os

from databases import Database
from sqlalchemy import (
    Column,
    Integer,
    MetaData,
    String,
    Table,
    Text,
    create_engine,
)

from app.core import config

# SQLAlchemy

engine = create_engine(config.config.pg_dsn)
metadata = MetaData()
posts = Table(
    "posts",
    metadata,
    Column("id", Integer, unique=True, index=True),
    Column("user_id", Integer),
    Column("title", String(255)),
    Column("body", Text),
)

comments = Table(
    "comments",
    metadata,
    Column("id", Integer, unique=True, index=True),
    Column("post_id", Integer, index=True),
    Column("name", String(255)),
    Column("email", String(255)),
    Column("body", Text),

)

# databases query builder
database = Database(config.config.pg_dsn)
