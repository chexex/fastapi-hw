import aioredis
from fastapi import FastAPI
from fastapi_cache import FastAPICache
from fastapi_cache.backends.redis import RedisBackend

from app.api.endpoints import h_check, posts
from app.core.config import config
from app.core.db import database, engine, metadata


def create_api():
    metadata.create_all(engine)

    api = FastAPI()

    api.include_router(h_check.router, tags=["health check"])
    # api.include_router(posts.router, prefix="/posts", tags=["posts"])
    api.include_router(posts.router, tags=["posts"])

    @api.on_event("startup")
    async def startup():
        redis = await aioredis.create_redis_pool(config.redis_dsn, encoding="utf8")
        FastAPICache.init(RedisBackend(redis), prefix=config.service_name)
        await database.connect()

    @api.on_event("shutdown")
    async def shutdown():
        await database.disconnect()

    return api
