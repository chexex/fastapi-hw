import asyncio
from contextlib import suppress
from typing import List, Optional

import asyncpg
import requests
import typer
from pydantic import BaseModel, Field

POSTS_URL = "https://jsonplaceholder.typicode.com/posts"
COMMENTS_URL = "https://jsonplaceholder.typicode.com/comments"


POSTS_INSERT_QUERY = (
    "INSERT INTO posts(id, user_id, title, body) VALUES($1, $2, $3, $4) ON CONFLICT DO NOTHING;"
)
COMMENTS_INSERT_QUERY = (
    "INSERT INTO comments(id, post_id, name, email, body) VALUES($1, $2, $3, $4, $5) ON CONFLICT DO NOTHING;"
)


class Post(BaseModel):
    id: int
    user_id: int = Field(alias="userId")
    title: str
    body: str

    def to_pg_values(self):
        return self.id, self.user_id, self.title, self.body


class Comment(BaseModel):
    id: int
    post_id: int = Field(alias="postId")
    name: str
    email: str
    body: str

    def to_pg_values(self):
        return self.id, self.post_id, self.name, self.email, self.body


def get_data(url):
    resp = requests.get(url, timeout=4)
    resp.raise_for_status()
    return resp.json()


def get_comments() -> List[Comment]:
    comments = get_data(COMMENTS_URL)
    return [Comment(**c) for c in comments]


def get_posts() -> List[Post]:
    posts = get_data(POSTS_URL)
    return [Post(**p) for p in posts]


async def save_to_pg(posts: List[Post], comments: List[Comment]):
    conn = await asyncpg.connect("postgresql://user:pass@db:5432/posts")
    await conn.executemany(POSTS_INSERT_QUERY, [p.to_pg_values() for p in posts])
    typer.echo("Posts has been successfully saved!")
    await conn.executemany(COMMENTS_INSERT_QUERY, [c.to_pg_values() for c in comments])
    typer.echo("Comments has been successfully saved!")


def main():
    typer.echo("Data fetch has been started")

    try:
        posts = get_posts()
    except requests.RequestException as e:
        typer.echo(f"Failed to download posts. Traceback: {e}", err=True)
        raise typer.Abort()

    try:
        comments = get_comments()
    except requests.RequestException:
        typer.echo(f"Failed to download comments. Traceback: {e}", err=True)
        raise typer.Abort()
    typer.echo("Data has been fetched.")
    typer.echo("Saving data to db has been enqueued.")
    asyncio.run(save_to_pg(posts, comments))


if __name__ == "__main__":
    typer.run(main)
