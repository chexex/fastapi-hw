# Posts test project

##  Description

* There are users, writing posts, provided by a service:  https://jsonplaceholder.typicode.com/posts 
* There are users, commenting on these posts, provided by a service: https://jsonplaceholder.typicode.com/comments

##  Getting Started

1. Clone the project
2. cd into the project
3. docker-compose -f docker-compose.yaml -f docker-compose-data.yaml up -d
4. open [localhost:5050](http://localhost:5050)

```bash
$ git clone git@gitlab.com:chexex/fastapi-hw.git

$ cd fastapi-hw

$ docker-compose -f docker-compose.yaml -f docker-compose-data.yaml up -d

$ open localhost:5050 
```

## Features

- spec
- reverse proxy via nginx
- cached views via redis
- deployment via docker containers
- configurable storages: postgres, redis

## Documentation

You can find spec docs at [http://localhost:5050/docs](http://localhost:5050/docs).


## Tests & Linters

Easy to run:

1. Build docker-compose with dev flag:
2. Run `api` container
3. Execute tests within container

```bash
docker-compose build --build-arg INSTALL_DEV=true --no-cache api

docker-compose up -d api

docker-compose exec api pytest tests
```

Developer way:

From project directory `fastapi-hw` run tox for tests & linters

```bash
$ tox
```

That is you have to have all the required dependencies on a host machine.
