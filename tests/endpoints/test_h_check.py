from fastapi.testclient import TestClient


def test_h_check(test_app: TestClient):
    res = test_app.get('/h-check')

    assert res.status_code == 200
    assert res.json()['status'] == 'OK'
