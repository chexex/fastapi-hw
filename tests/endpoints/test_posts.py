from collections import defaultdict

import pytest

from app.api import crud
from app.api.endpoints.posts import get_post_to_comments, get_post_with_comments
from app.api.models import CommentDB, PostDB, PostWithCommentsDB


@pytest.mark.parametrize(
    ["data"],
    [
        (
            [
                {"id": 1, "post_id": 1, "name": "name", "email": "a@devmail.com", "body": "body"},
                {
                    "id": 2,
                    "post_id": 1,
                    "name": "name2",
                    "email": "a2@devmail.com",
                    "body": "body2",
                },
                {
                    "id": 3,
                    "post_id": 2,
                    "name": "name3",
                    "email": "a3@devmail.com",
                    "body": "body3",
                },
            ],
        ),
        ([{"id": 1, "post_id": 1, "name": "name", "email": "a@devmail.com", "body": "body"}],),
    ],
)
def test_get_post_to_comments(data):
    comments = [CommentDB(**d) for d in data]
    post_to_comments = defaultdict(list)
    for c in comments:
        post_to_comments[c.post_id].append(c)
    assert post_to_comments == get_post_to_comments(comments)


def test_get_post_with_comments():
    post_data = {
        "user_id": 1,
        "title": "test title",
        "body": "test body",
        "id": 1,
    }
    comments_data = [
        {
            "post_id": 1,
            "name": "id labore ex et quam laborum",
            "email": "Eliseo@gardner.biz",
            "body": "laudantium enim quasi est quidem magnam voluptate ipsam eos",
            "id": 1,
        },
        {
            "post_id": 1,
            "name": "quo vero reiciendis velit similique earum",
            "email": "Jayne_Kuhic@sydney.com",
            "body": "est natus enim nihil est dolore omnis voluptatem numquam",
            "id": 2,
        },
    ]
    post = PostDB(**post_data)
    comments = [CommentDB(**c) for c in comments_data]
    expected = PostWithCommentsDB(
        **post_data,
        comments=comments,
    )
    assert get_post_with_comments(post, comments) == expected


def test_read_post(test_app, monkeypatch):
    post_sample = {
        "user_id": 1,
        "title": "test title",
        "body": "test body",
        "id": 1,
    }
    comments_sample = [
            {
                "post_id": 1,
                "name": "id labore ex et quam laborum",
                "email": "Eliseo@gardner.biz",
                "body": "laudantium enim quasi est quidem magnam voluptate ipsam eos",
                "id": 1,
            },
            {
                "post_id": 1,
                "name": "quo vero reiciendis velit similique earum",
                "email": "Jayne_Kuhic@sydney.com",
                "body": "est natus enim nihil est dolore omnis voluptatem numquam",
                "id": 2,
            },
        ]

    async def mock_get_post_by_id(id_):
        return post_sample

    async def mock_get_comments_by_post_id(*args, **kwargs):
        return comments_sample

    monkeypatch.setattr(crud, "get_post_by_id", mock_get_post_by_id)
    monkeypatch.setattr(crud, "get_comments_by_post_id", mock_get_comments_by_post_id)

    response = test_app.get("/1")

    expected = {
        **post_sample,
        "comments": comments_sample,
    }
    assert response.status_code == 200
    assert response.json() == expected


def test_read_post_incorrect_id(test_app, monkeypatch):
    async def mock_get_post_by_id(id_):
        return None

    monkeypatch.setattr(crud, "get_post_by_id", mock_get_post_by_id)

    response = test_app.get("/99999")
    assert response.status_code == 404
    assert response.json()["detail"] == "Post not found"

    response = test_app.get("/0")
    assert response.status_code == 422


def test_read_all_posts(test_app, monkeypatch):
    posts_sample = [
        {
            "user_id": 1,
            "title": "test title",
            "body": "test body",
            "id": 1,
        }
    ]

    comments_sample = [
        {
            "post_id": 1,
            "name": "id labore ex et quam laborum",
            "email": "Eliseo@gardner.biz",
            "body": "laudantium enim quasi est quidem magnam voluptate ipsam eos\ntempora quo necessitatibus\ndolor quam autem quasi\nreiciendis et nam sapiente accusantium",
            "id": 1,
        },
        {
            "post_id": 1,
            "name": "quo vero reiciendis velit similique earum",
            "email": "Jayne_Kuhic@sydney.com",
            "body": "est natus enim nihil est dolore omnis voluptatem numquam\net omnis occaecati quod ullam at\nvoluptatem error expedita pariatur\nnihil sint nostrum voluptatem reiciendis et",
            "id": 2,
        },
    ]

    async def mock_get_posts(*args, **kwargs):
        return posts_sample

    async def mock_get_comments(*args, **kwargs):
        return comments_sample

    monkeypatch.setattr(crud, "get_posts", mock_get_posts)
    monkeypatch.setattr(crud, "get_comments", mock_get_comments)

    response = test_app.get("/")

    expected = [
        {
            **posts_sample[0],
            "comments": comments_sample,
        }
    ]
    assert response.status_code == 200
    assert response.json() == expected
